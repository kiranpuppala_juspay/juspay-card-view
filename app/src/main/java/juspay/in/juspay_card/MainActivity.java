package juspay.in.juspay_card;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import in.juspay.juspaycardview.ApiCallback;
import in.juspay.juspaycardview.JuspayCardView;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "MAINACTIVITY";
    private TextView tokenView,cardText;
    private JuspayCardView card;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        card = findViewById(R.id.cardView);
        tokenView = findViewById(R.id.token);
        cardText = findViewById(R.id.cardText);
    }

    public void getToken(View view) {
        card.getToken(new ApiCallback() {
            @Override
            public void onResponse(String response) {
                response = response != null ? response : "null response";
                Toast.makeText(MainActivity.this,"TOKEN "+response,Toast.LENGTH_SHORT).show();
                Log.e(LOG_TAG,response);
                tokenView.setText(response);
            }
        });
    }

    public void getCard(View view) {
        cardText.setText(card.getCard().toString());
    }
}
