package in.juspay.juspaycardview;

public interface ApiCallback {
    public void onResponse(String response);
}
