package in.juspay.juspaycardview;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class ApiClient extends AsyncTask<Void, Void, String> {
    private final String REQUEST_METHOD = "POST";
    private final String API_URL = "https://sandbox.juspay.in/card/tokenize";
    private final int READ_TIMEOUT = 15000;
    private final int CONNECTION_TIMEOUT = 15000;
    public ResponseCallback responseCallback;
    public Map<String, String> requestPayload = new HashMap<>();

    public interface ResponseCallback{
        public void onResponse(String response);
    }

    public ApiClient(Map<String,String> payload,ResponseCallback callback){
        this.responseCallback=callback;
        this.requestPayload=payload;
    }

    @Override
    protected String doInBackground(Void... params){
        String stringUrl = API_URL;
        String result;
        try {
            HttpsURLConnection connection = (HttpsURLConnection) (new URL(stringUrl).openConnection());

            connection.setRequestMethod(REQUEST_METHOD);
            connection.setReadTimeout(READ_TIMEOUT);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);
//            connection.setDoOutput(true);
            connection.setSSLSocketFactory(new TLSSocketFactory());
            OutputStream stream = connection.getOutputStream();
            stream.write(generateQueryString(this.requestPayload).getBytes());
            result = readInput(connection);
        }
        catch(IOException e){
            e.printStackTrace();
            result = null;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            result=null;
        } catch (KeyManagementException e) {
            e.printStackTrace();
            result=null;
        }
        return result;
    }
    protected void onPostExecute(String result){
        this.responseCallback.onResponse(result);
    }

    public static String generateQueryString(Map<String, String> queryString) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        Map.Entry e;
        for(Iterator var2 = queryString.entrySet().iterator(); var2.hasNext(); sb.append(URLEncoder.encode((String)e.getKey(), "UTF-8")).append('=').append(URLEncoder.encode((String)e.getValue(), "UTF-8"))) {
            e = (Map.Entry)var2.next();
            if(sb.length() > 0) {
                sb.append('&');
            }
        }
        return sb.toString();
    }

    public  String readInput (HttpURLConnection connection) throws IOException {
        int responseCode = connection.getResponseCode();
        Map<String, List<String>> headerFields = connection.getHeaderFields();
        InputStreamReader responseReader;
        if((responseCode < 200 || responseCode >= 300) && responseCode != 302) {
            responseReader = new InputStreamReader(connection.getErrorStream());
        } else {
            responseReader = new InputStreamReader(connection.getInputStream());
        }

        BufferedReader in = new BufferedReader(responseReader);
        StringBuilder response = new StringBuilder();

        String inputLine;
        while((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        connection.disconnect();
        return  response.toString();
    }

    class TLSSocketFactory extends SSLSocketFactory {

        private SSLSocketFactory delegate;

        public TLSSocketFactory() throws KeyManagementException, NoSuchAlgorithmException {
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, null, null);
            delegate = context.getSocketFactory();
        }

        @Override
        public String[] getDefaultCipherSuites() {
            return delegate.getDefaultCipherSuites();
        }

        @Override
        public String[] getSupportedCipherSuites() {
            return delegate.getSupportedCipherSuites();
        }

        @Override
        public Socket createSocket() throws IOException {
            return enableTLSOnSocket(delegate.createSocket());
        }

        @Override
        public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
            return enableTLSOnSocket(delegate.createSocket(s, host, port, autoClose));
        }

        @Override
        public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
            return enableTLSOnSocket(delegate.createSocket(host, port));
        }

        @Override
        public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException, UnknownHostException {
            return enableTLSOnSocket(delegate.createSocket(host, port, localHost, localPort));
        }

        @Override
        public Socket createSocket(InetAddress host, int port) throws IOException {
            return enableTLSOnSocket(delegate.createSocket(host, port));
        }

        @Override
        public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
            return enableTLSOnSocket(delegate.createSocket(address, port, localAddress, localPort));
        }

        private Socket enableTLSOnSocket(Socket socket) {
            if(socket != null && (socket instanceof SSLSocket)) {
                ((SSLSocket)socket).setEnabledProtocols(new String[] {"TLSv1.1", "TLSv1.2"});
            }
            return socket;
        }

    }



}

