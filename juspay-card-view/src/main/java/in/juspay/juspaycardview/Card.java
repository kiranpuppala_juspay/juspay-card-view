package in.juspay.juspaycardview;

public class Card {

    private String cardNumber;
    private String cardExpMonth;
    private String  cardExpYear;
    private String  cardCVV;

    public Card(String cardNumber, String cardExpMonth, String cardExpYear, String cardCVV){
        this.cardNumber=cardNumber;
        this.cardExpMonth=cardExpMonth;
        this.cardExpYear=cardExpYear;
        this.cardCVV=cardCVV;
    }

    public Card(){
    }

    public void setCardNumber(String cardNumber){
        this.cardNumber=cardNumber;
    }

    public void setCardExpMonthd(String cardExpMonth){
        this.cardNumber=cardExpMonth;
    }

    public void setCardExpYear(String cardExpYear){
        this.cardNumber=cardNumber;
    }

    public void setCardCVV(String cardCVV){
        this.cardNumber=cardNumber;
    }

    public String getCardNumber(){
        return this.cardNumber;
    }

    public String getCardExpMonth(){
        return this.cardExpMonth;
    }

    public String getCardExpYear(){
        return this.cardExpYear;
    }

    public String getCardCVV(){
        return this.cardCVV;
    }

    public void validateCard(){

    }

    public String toString(){
        String delimter = ",";
        return this.cardNumber+delimter+this.cardExpMonth+delimter+this.cardExpYear+delimter+this.cardCVV;
    }
}