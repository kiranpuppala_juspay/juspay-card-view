package in.juspay.juspaycardview;

import java.util.HashMap;
import java.util.Map;

public class CardUtils {
    public static final Map<String, String> cardPattern = new HashMap<String, String>() {{
        put("visa", "^4[0-9]{6,}$");
        put("mastercard", "^5[1-5][0-9]{5,}$");
        put("americanexpress", "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$");
        put("dinersclub", "^6(?:011|5[0-9]{2})[0-9]{3,}$");
        put("discover", "^6(?:011|5[0-9]{2})[0-9]{3,}$");
        put("jcb", "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$");
    }};
}
