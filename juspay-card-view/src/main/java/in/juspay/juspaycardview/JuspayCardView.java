package in.juspay.juspaycardview;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.Map;

public class JuspayCardView extends LinearLayout {
    private static final int CARD_NUM_LENGTH = 19;
    private static final int CARD_EXP_LENGTH = 5;
    private static final int CARD_CVV_LENGTH = 3;
    private static final char space = ' ';
    private static final char slash = '/';
    private String LOG_TAG = "JUSPAYCARDVIEW";
    private int textColorInt;
    private int hintColorInt;
    private int errorColorInt;
    private ImageView cardLogo;
    private EditText cardNumber;
    private EditText cardExpiry;
    private EditText cardCvv;
    private TextWatcher textChangeListener = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {

            if (s == cardNumber.getEditableText()) {
                insertDelimiter(s, space, 4);
                if (cardNumber.getText().length() == CARD_NUM_LENGTH) cardExpiry.requestFocus();
                inflateCardLogo(validateCard(s.toString().replaceAll("\\s+", "")));

            } else if (s == cardExpiry.getEditableText()) {
                insertDelimiter(s, slash, 2);
                if (cardExpiry.getText().length() == CARD_EXP_LENGTH) cardCvv.requestFocus();
                if (cardExpiry.getText().length() == 0) cardNumber.requestFocus();

            } else if (s == cardCvv.getEditableText()) {
                if (cardCvv.getText().length() == 0) cardExpiry.requestFocus();
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start,
                                      int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {

        }
    };

    public JuspayCardView(Context context) {
        super(context);
        initView(null);
    }

    public JuspayCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(attrs);
    }

    public JuspayCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(attrs);
    }

    public void initView(AttributeSet attrs) {
        inflate(getContext(), R.layout.card_widget, this);
        setOrientation(LinearLayout.HORIZONTAL);
        if (attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.JuspayCardView,
                    0, 0);
            try {
                textColorInt =
                        a.getColor(R.styleable.JuspayCardView_textColor, textColorInt);
                hintColorInt =
                        a.getColor(R.styleable.JuspayCardView_hintColor, hintColorInt);
                errorColorInt =
                        a.getColor(R.styleable.JuspayCardView_errorColor, errorColorInt);

            } finally {
                a.recycle();
            }
        }

        cardNumber = findViewById(R.id.cardNumber);
        cardExpiry = findViewById(R.id.cardExpiry);
        cardCvv = findViewById(R.id.cardCvv);
        cardLogo = findViewById(R.id.cardLogo);

        cardNumber.setTextColor(textColorInt);
        cardExpiry.setTextColor(textColorInt);
        cardCvv.setTextColor(textColorInt);

        cardNumber.setHintTextColor(hintColorInt);
        cardExpiry.setHintTextColor(hintColorInt);
        cardCvv.setHintTextColor(hintColorInt);

        cardNumber.addTextChangedListener(textChangeListener);
        cardExpiry.addTextChangedListener(textChangeListener);
        cardCvv.addTextChangedListener(textChangeListener);

    }

    private void insertDelimiter(Editable s, char delimiter, int interval) {
        if (s.length() > 0 && (s.length() % (interval + 1)) == 0) {
            final char c = s.charAt(s.length() - 1);
            if (delimiter == c) {
                s.delete(s.length() - 1, s.length());
            }
        }
        if (s.length() > 0 && (s.length() % (interval + 1)) == 0) {
            char c = s.charAt(s.length() - 1);
            if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(delimiter)).length <= interval - 1) {
                s.insert(s.length() - 1, String.valueOf(delimiter));
            }
        }
    }

    private void inflateCardLogo(String cardName) {
        int resourceId = getResources().getIdentifier("ic_" + cardName, "drawable", cardLogo.getContext().getPackageName());
        cardLogo.setImageResource(resourceId);
    }

    public Card getCard() {
        String expiry[] = cardExpiry.getText().toString().split("/");
        try {
            Card card = new Card(cardNumber.getText().toString().replaceAll("\\s+", ""), expiry[0], expiry[1], cardCvv.getText().toString());
            return card;
        }catch (Exception e){
            return new Card();
        }
    }

    public String validateCard(final String cardNum) {
        for (Map.Entry<String, String> entry : CardUtils.cardPattern.entrySet()) {
            if (cardNum.matches(entry.getValue()))
                return entry.getKey();
        }
        return "card";
    }

    public void getToken(final ApiCallback callback){
        Map<String, String> requestPayload = new HashMap<>();
        requestPayload.put("card_number",getCard().getCardNumber()+"");
        requestPayload.put("card_exp_year",getCard().getCardExpYear()+"");
        requestPayload.put("card_exp_month",getCard().getCardExpMonth()+"");
        requestPayload.put("card_security_code",getCard().getCardCVV()+"");
        requestPayload.put("merchant_id","foodpanda");
        requestPayload.put("name_on_card","abc");
        new ApiClient(requestPayload,new ApiClient.ResponseCallback() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(response);
            }
        }).execute();
    }

}
